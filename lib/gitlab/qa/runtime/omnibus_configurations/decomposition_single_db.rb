# frozen_string_literal: true

module Gitlab
  module QA
    module Runtime
      module OmnibusConfigurations
        class DecompositionSingleDb < Default
          def configuration
            <<~OMNIBUS
              gitlab_rails['databases']['main']['enable'] = true
              gitlab_rails['databases']['ci']['enable'] = true
              gitlab_rails['databases']['ci']['db_database'] = 'gitlabhq_production'
            OMNIBUS
          end
        end
      end
    end
  end
end
