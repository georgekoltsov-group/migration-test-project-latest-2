# frozen_string_literal: true

module Gitlab
  module QA
    VERSION = '8.5.0'
  end
end
