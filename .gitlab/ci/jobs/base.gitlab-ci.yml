stages:
  - test
  - report
  - notify

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-qa-ruby-2.7
  tags:
    - gitlab-org
  cache:
    key:
      files:
        - Gemfile.lock
        - gitlab-qa.gemspec
    paths:
      - vendor/ruby
  before_script:
    - export QA_ARTIFACTS_DIR=$CI_PROJECT_DIR
    - bundle version
    - bundle config path vendor
    - bundle install --jobs=$(nproc) --retry=3 --quiet && bundle check
    - export LANG=C.UTF-8
    - echo $CI_MERGE_REQUEST_IID
    - echo $CI_COMMIT_BRANCH
    - echo $CI_COMMIT_TAG
    - echo $CI_PIPELINE_SOURCE
    - echo $CI_DEFAULT_BRANCH
    - echo $CI_COMMIT_REF_NAME
    - echo $CI_JOB_ID
    - echo $CI_COMMIT_REF_PROTECTED
    - echo $TOP_UPSTREAM_SOURCE_SHA
    - echo $TOP_UPSTREAM_MERGE_REQUEST_IID
    - echo $TOP_UPSTREAM_SOURCE_PROJECT
    - echo $TOP_UPSTREAM_DEFAULT_BRANCH
    - echo $TOP_UPSTREAM_SOURCE_REF
    - echo $TOP_UPSTREAM_SOURCE_JOB
    - echo $GITLAB_QA_OPTIONS
    - echo $QA_TESTS
    - echo $RSPEC_REPORT_OPTS
    - echo $QA_RSPEC_TAGS
    - echo $QA_TEST_RESULTS_ISSUES_PROJECT
    - echo $QA_TESTCASES_REPORTING_PROJECT
    - echo $DEFAULT_RELEASE
    - echo $GITLAB_QA_OPTIONS_COMBINED
    - echo $DISABLE_RELATING_FAILURE_ISSUES
    - echo $QA_FAILURES_MAX_DIFF_RATIO
    - echo $QA_FAILURES_REPORTER_OPTIONS
    - echo $NOTIFY_CHANNEL
    - echo $REPORT_ISSUE_URL
    - echo $QA_TESTCASE_SESSIONS_PROJECT
    - echo $QA_FAILURES_REPORTING_PROJECT
    - echo $ALLURE_JOB_NAME
    - echo $QA_IMAGE
    - echo $RELEASE
    - echo $TEST_LICENSE_MODE

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: $CI_MERGE_REQUEST_IID
    # For the default branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: $CI_COMMIT_BRANCH == "master"
    # For tags, create a pipeline.
    - if: $CI_COMMIT_TAG
    # For triggers from GitLab MR pipelines (and pipelines from other projects or parent pipeline), create a pipeline
    - if: $CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "parent_pipeline"
    # When using Run pipeline button in the GitLab UI, from the project’s CI/CD > Pipelines section, create a pipeline.
    - if: $CI_PIPELINE_SOURCE == "web"

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375
  COLORIZED_LOGS: "true"
  QA_CAN_TEST_GIT_PROTOCOL_V2: "true"
  QA_CAN_TEST_PRAEFECT: "false"
  QA_GENERATE_ALLURE_REPORT: "true"
  QA_TESTCASES_REPORTING_PROJECT: "gitlab-org/gitlab"
  QA_TEST_RESULTS_ISSUES_PROJECT: "gitlab-org/quality/testcases"
  QA_TESTCASE_SESSIONS_PROJECT: "gitlab-org/quality/testcase-sessions"
  # QA_DEFAULT_BRANCH is the default branch name of the instance under test.
  QA_DEFAULT_BRANCH: "master"

.ce-variables:
  variables:
    DEFAULT_RELEASE: "CE"

.ee-variables:
  variables:
    DEFAULT_RELEASE: "EE"

.test:
  stage: test
  timeout: 1 hour 30 minutes
  services:
    - docker:20.10.5-dind
  tags:
    - gitlab-org
    - docker
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - gitlab-qa-run-*
    reports:
      junit: gitlab-qa-run-*/**/rspec-*.xml
  script:
    - 'echo "QA_ARTIFACTS_DIR: $QA_ARTIFACTS_DIR"'
    - 'echo "CI_PROJECT_DIR: $CI_PROJECT_DIR"'
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS || test_run_exit_code=$?
    - bundle exec exe/gitlab-qa-report --update-screenshot-path "gitlab-qa-run-*/**/rspec-*.xml"
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - if [ "$TOP_UPSTREAM_SOURCE_REF" == $TOP_UPSTREAM_DEFAULT_BRANCH ] || [[ "$TOP_UPSTREAM_SOURCE_JOB" == https://ops.gitlab.net* ]]; then exe/gitlab-qa-report --report-results "gitlab-qa-run-*/**/rspec-*.json" --test-case-project "$QA_TESTCASES_REPORTING_PROJECT" --results-issue-project "$QA_TEST_RESULTS_ISSUES_PROJECT" || true; fi
    - exit $test_run_exit_code

# For jobs that shouldn't report results in issues, e.g., manually run custom jobs
.no-issue-report-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS

# For jobs that provide additional GITLAB_QA_OPTIONS, e.g., jobs that include --omnibus-config
.combined-gitlab-qa-options-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS_COMBINED -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'

    - bundle exec exe/gitlab-qa ${QA_SCENARIO:=Test::Instance::Image} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS_COMBINED -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS || test_run_exit_code=$?
    - bundle exec exe/gitlab-qa-report --update-screenshot-path "gitlab-qa-run-*/**/rspec-*.xml"
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - if [ "$TOP_UPSTREAM_SOURCE_REF" == $TOP_UPSTREAM_DEFAULT_BRANCH ] || [[ "$TOP_UPSTREAM_SOURCE_JOB" == https://ops.gitlab.net* ]]; then exe/gitlab-qa-report --report-results "gitlab-qa-run-*/**/rspec-*.json" --test-case-project "$QA_TESTCASES_REPORTING_PROJECT" --results-issue-project "$QA_TEST_RESULTS_ISSUES_PROJECT" || true; fi
    - exit $test_run_exit_code

# The Test::Omnibus::Update scenarios require the release to be specified twice, which can't be done dynamically using the `variables` parameter
# So instead we can use this script with two release variables
.update-scenario-script:
  script:
    - 'echo "Running: bundle exec exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=$DEFAULT_RELEASE} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS"'
    - bundle exec exe/gitlab-qa Test::Omnibus::Update ${RELEASE:=$DEFAULT_RELEASE} ${RELEASE:=$DEFAULT_RELEASE} $GITLAB_QA_OPTIONS -- $QA_TESTS $QA_RSPEC_TAGS $RSPEC_REPORT_OPTS || test_run_exit_code=$?
    - bundle exec exe/gitlab-qa-report --update-screenshot-path "gitlab-qa-run-*/**/rspec-*.xml"
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - if [ "$TOP_UPSTREAM_SOURCE_REF" == $TOP_UPSTREAM_DEFAULT_BRANCH ] || [[ "$TOP_UPSTREAM_SOURCE_JOB" == https://ops.gitlab.net* ]]; then exe/gitlab-qa-report --report-results "gitlab-qa-run-*/**/rspec-*.json" --test-case-project "$QA_TESTCASES_REPORTING_PROJECT" --results-issue-project "$QA_TEST_RESULTS_ISSUES_PROJECT" || true; fi
    - exit $test_run_exit_code

.high-capacity:
  tags:
    - e2e

.rspec-report-opts:
  variables:
    FILE_SAFE_JOB_NAME: $(echo $CI_JOB_NAME | sed 's/[ /]/_/g')
    RSPEC_REPORT_OPTS: '--format QA::Support::JsonFormatter --out "tmp/rspec-${CI_JOB_ID}.json" --format RspecJunitFormatter --out "tmp/rspec-${CI_JOB_ID}.xml" --format html --out "tmp/rspec-${FILE_SAFE_JOB_NAME}.htm" --color --format documentation'

.quarantine:
  allow_failure: true
  variables:
    QA_RSPEC_TAGS: "--tag quarantine"

generate_test_session:
  stage: report
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_JOB !=null && $TOP_UPSTREAM_SOURCE_JOB !="" && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: always
    - if: '$TOP_UPSTREAM_SOURCE_JOB =~ /\Ahttps:\/\/ops.gitlab.net\//'
      when: always
  artifacts:
    when: always
    expire_in: 10d
    paths:
      - REPORT_ISSUE_URL
  script:
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - bundle exec exe/gitlab-qa-report --generate-test-session "gitlab-qa-run-*/**/rspec-*.json" --project "$QA_TESTCASE_SESSIONS_PROJECT"

relate_test_failures:
  stage: report
  rules:
    - if: '$DISABLE_RELATING_FAILURE_ISSUES != null && $DISABLE_RELATING_FAILURE_ISSUES != ""'
      when: never
    - if: '$TOP_UPSTREAM_SOURCE_JOB !=null && $TOP_UPSTREAM_SOURCE_JOB !="" && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: always
    - if: '$TOP_UPSTREAM_SOURCE_JOB =~ /\Ahttps:\/\/ops.gitlab.net\//'
      when: always
  variables:
    QA_FAILURES_REPORTING_PROJECT: "gitlab-org/gitlab"
    QA_FAILURES_MAX_DIFF_RATIO: "0.15"
    # The --dry-run can be set to modify the behavior of `exe/gitlab-qa-report --relate-failure-issue` without releasing a new gem version.
    QA_FAILURES_REPORTER_OPTIONS: ""
  script:
    - export GITLAB_QA_ACCESS_TOKEN="$GITLAB_QA_PRODUCTION_ACCESS_TOKEN"
    - bundle exec exe/gitlab-qa-report --relate-failure-issue "gitlab-qa-run-*/**/rspec-*.json" --project "$QA_FAILURES_REPORTING_PROJECT" --max-diff-ratio "$QA_FAILURES_MAX_DIFF_RATIO" $QA_FAILURES_REPORTER_OPTIONS

.notify_upstream:
  stage: notify
  image: ruby:3.0-alpine
  dependencies: []
  before_script:
    - gem install gitlab --no-document

notify_upstream:success:
  extends: .notify_upstream
  script:
    - bin/notify_upstream success
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_PROJECT && $TOP_UPSTREAM_MERGE_REQUEST_IID && $TOP_UPSTREAM_SOURCE_SHA'
      when: on_success

notify_upstream:failure:
  extends: .notify_upstream
  script:
    - bin/notify_upstream failure
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_PROJECT && $TOP_UPSTREAM_MERGE_REQUEST_IID && $TOP_UPSTREAM_SOURCE_SHA'
      when: on_failure

.notify_slack:
  image: alpine
  stage: notify
  dependencies: ['generate_test_session']
  cache: {}
  before_script:
    - apk update && apk add git curl bash

notify_slack:
  extends:
    - .notify_slack
  rules:
    - if: '$TOP_UPSTREAM_SOURCE_JOB !=null && $TOP_UPSTREAM_SOURCE_JOB !="" && $NOTIFY_CHANNEL !=null && $NOTIFY_CHANNEL !=""'
      when: on_failure
    - if: '$TOP_UPSTREAM_SOURCE_JOB !=null && $TOP_UPSTREAM_SOURCE_JOB !="" && $TOP_UPSTREAM_SOURCE_REF == $TOP_UPSTREAM_DEFAULT_BRANCH'
      when: on_failure
  script:
    - export RELEASE=${TOP_UPSTREAM_SOURCE_REF:-$RELEASE}
    - echo "NOTIFY_CHANNEL is ${NOTIFY_CHANNEL:=qa-$TOP_UPSTREAM_SOURCE_REF}"
    - echo "RELEASE is ${RELEASE}"
    - echo "CI_PIPELINE_URL is $CI_PIPELINE_URL"
    - echo "TOP_UPSTREAM_SOURCE_JOB is $TOP_UPSTREAM_SOURCE_JOB"
    - 'bin/slack $NOTIFY_CHANNEL "☠️ QA against $RELEASE failed! ☠️ See the test session report: $(cat REPORT_ISSUE_URL), and pipeline: $CI_PIPELINE_URL (triggered from $TOP_UPSTREAM_SOURCE_JOB)" ci_failing'

generate-allure-report:
  extends: .generate-allure-report-base
  stage: report
  variables:
    ALLURE_RESULTS_GLOB: gitlab-qa-run-*/**/allure-results/*
  cache:
    policy: pull
  before_script:
    - export STORAGE_CREDENTIALS=$QA_ALLURE_REPORT_GCS_CREDENTIALS
    - export GITLAB_AUTH_TOKEN=$GITLAB_QA_MR_ALLURE_REPORT_TOKEN
    # Set allure variables for merge request comment based on pipeline source
    - export ALLURE_MERGE_REQUEST_IID=${TOP_UPSTREAM_MERGE_REQUEST_IID:-$CI_MERGE_REQUEST_IID}
    - export ALLURE_PROJECT_PATH=${TOP_UPSTREAM_SOURCE_PROJECT:-$CI_MERGE_REQUEST_PROJECT_PATH}
    - export ALLURE_COMMIT_SHA=${TOP_UPSTREAM_SOURCE_SHA:-$CI_MERGE_REQUEST_SOURCE_BRANCH_SHA}
  rules:
    # Don't run report generation on release pipelines
    - if: '$CI_SERVER_HOST == "gitlab.com" && $CI_PROJECT_PATH == "gitlab-org/gitlab-qa" && $RELEASE == null'
      changes: ["lib/**/version.rb"]
      when: never
    - if: '$CI_COMMIT_TAG'
      when: never
    - if: '$ALLURE_JOB_NAME'
      when: always

generate-knapsack-report:
  extends: .generate-knapsack-report-base
  image:
    name: ${QA_IMAGE}
  stage: report

include:
  - local: .gitlab/ci/rules.gitlab-ci.yml
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/allure-report.yml'
      - '/ci/knapsack-report.yml'
